/*
 * IntoMPI.4.c: matrix multiplication using MPI.
 * There are some simplifications here. The main one is that matrices B and C 
 * are fully allocated everywhere, even though only a portion of them is 
 * used by each processor (except for processor 0)
 */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <mpi.h>

#define SIZE 2			/* Size of matrices */

// global arrays

int A[SIZE][SIZE], B[SIZE][SIZE], C[SIZE][SIZE];

// function prototypes

void fill_matrix(int m[SIZE][SIZE]);
void print_matrix(int m[SIZE][SIZE]);

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

int main(int argc, char *argv[])
{
  int myid, ncores, root, from, to, i, j, k;
  int tag = 42;		/* any value will do */

  MPI_Status status;

  // initialize MPI
  MPI_Init (&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &myid);	  /* who am i */
  MPI_Comm_size(MPI_COMM_WORLD, &ncores);      /* number of processors */

  // set the root process
  root = 0;

  /* Just to use the simple variants of MPI_Gather and MPI_Scatter we */
  /* impose that SIZE is divisible by P. By using the vector versions, */
  /* (MPI_Gatherv and MPI_Scatterv) it is easy to drop this restriction. */

  if (SIZE%ncores!=0) {
    if (myid==root)
      {
	printf("Matrix size not divisible by number of processors\n");
      }
    MPI_Finalize();
    exit(0);
  }

  from = myid * SIZE/ncores;
  to = (myid+1) * SIZE/ncores;

  /* Process 0 fills the input matrices and broadcasts them to the rest */
  /* (actually, only the relevant stripe of A is sent to each process) */

  if (myid==root)
    {
      fill_matrix(A);
      fill_matrix(B);
    }

  // root sends the matrix B to all cores.
  MPI_Bcast (B, SIZE*SIZE, MPI_INT, root, MPI_COMM_WORLD);  

  // root scatters the matrix A to all cores.  
  // note that since sendtype=recvtype, sendcount = recvcount
  MPI_Scatter (A, SIZE*SIZE/ncores, MPI_INT, A[from], SIZE*SIZE/ncores, MPI_INT, root, MPI_COMM_WORLD);

  printf("computing slice %d (from row %d to %d)\n", myid, from, to-1);
  for (i=from; i<to; i++) 
    for (j=0; j<SIZE; j++) {
      C[i][j]=0;
      for (k=0; k<SIZE; k++)
	C[i][j] += A[i][k]*B[k][j];
    }


  // retrieve the computations from each core back to the root
  MPI_Gather (C[from], SIZE*SIZE/ncores, MPI_INT, C, SIZE*SIZE/ncores, MPI_INT, root, MPI_COMM_WORLD);

  if (myid==root) {
    printf("\n\n");
    print_matrix(A);
    printf("\n\n\t       * \n");
    print_matrix(B);
    printf("\n\n\t       = \n");
    print_matrix(C);
    printf("\n\n");
  }

  MPI_Finalize();
  return 0;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// function to create a matrix that is built from a sequence of integers

void fill_matrix(int m[SIZE][SIZE])
{
  static int n=0;
  int i, j;
  for (i=0; i<SIZE; i++)
    for (j=0; j<SIZE; j++)
      m[i][j] = n++;
}

// function to print the resultant 

void print_matrix(int m[SIZE][SIZE])
{
  int i, j = 0;
  for (i=0; i<SIZE; i++) {
    printf("\n\t| ");
    for (j=0; j<SIZE; j++)
      printf("%2d ", m[i][j]);
    printf("|");
  }
}
