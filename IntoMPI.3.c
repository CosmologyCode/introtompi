/*
 *     SO - We need to compute arctan(1.0)
 *         Arctan(x) has a known derivative of x'/(1+x*x) = 1.0/(1+x*x)
 *         By integrating this, we can approximate Arctan(1.0)
 *         We'll perform a numerical integration to get an 
 *         approximation of Pi/4, then multiply by 4.0
 *         to get an approximation of Pi.
 */

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <mpi.h>

/* Compute the Derivative of ArcTan*/
double dx_arctan(double x)
{
  return (1.0 / (1.0 + x*x));
}

int main(int argc, char *argv[])
{
  int n, root;
    double PI25DT = 3.141592653589793238462643;
    double mypi, h, pi, i, sum, x, a, startwtime, endwtime;
    int myid, numprocs, resultlen;
    char name[MPI_MAX_PROCESSOR_NAME] ; 

    MPI_Init(&argc,&argv);

    /* Do this FIRST (to avoid error)  */    
    MPI_Comm_size(MPI_COMM_WORLD,&numprocs);
    MPI_Comm_rank(MPI_COMM_WORLD,&myid);

    /* Get Runtime information */
    root = 0;
    
    // number of intervals
    if(myid==root)
      n = 100000;
          
    /* Root Shares number of intervals with other processors */
    MPI_Bcast(&n, 1, MPI_INT, root, MPI_COMM_WORLD);

    sum = 0.0;
    h   = 1.0/n;

    /* Compute and Sum the "Heights" of each bar of the integration */
    for (i=myid+0.5; i<n; i+=numprocs)
      sum += dx_arctan(i*h);

    /* Multiply by the "Widths" of each bar and 4.0 (arctan(1)=Pi/4) */
    mypi = 4.0*h*sum;

    /* Consolidate and Sum Results */
    MPI_Reduce(&mypi, &pi, 1, MPI_DOUBLE, MPI_SUM, root, MPI_COMM_WORLD);

    if (myid == root)
      printf("pi is approximately %.16f, Error is %.16f\n",pi, fabs(pi - PI25DT)); 

    MPI_Finalize();
    return 0;
}
