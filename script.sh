#!/bin/bash -l

#SBATCH --ntasks 16 # The number of cores you need...

#SBATCH -J TEST1 #Give it something meaningful.

#SBATCH -o /cosma/home/dp004/dc-smit4/IntroMPIExampleCode/INFO/standard_output_file.%J.out

#SBATCH -e /cosma/home/dp004/dc-smit4/IntroMPIExampleCode/INFO/standard_error_file.%J.err

#SBATCH -p cosma7 #or some other partition, e.g. cosma, cosma6, etc.

#SBATCH -A dp004

#SBATCH --exclusive

#SBATCH -t 00:01:00

#SBATCH --mail-type=END # notifications for job done & fail

#SBATCH --mail-user=res33@sussex.ac.uk #PLEASE PUT YOUR EMAIL ADDRESS HERE (without the <>)

#module purge
#load the modules used to build your program.

module load gnu_comp/7.3.0

module load openmpi/3.0.1

# Run the program
cd /cosma/home/dp004/dc-smit4/IntroMPIExampleCode/

mpirun -np $SLURM_NTASKS IntoMPI.1.exe
