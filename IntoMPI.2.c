#include <stdio.h>
#include <string.h>
#include <mpi.h>

int main(int argc, char **argv)
{

  int i, root;
  int myid;        // rank of process
  int ncores;      // total number of cores
  int source;      // rank of sender 
  int dest;        // rank of reciever
  int tag = 1;     // tag for messages

#define MAX_STR_LEN 100  

  char message[MAX_STR_LEN]; // storage for messages

  MPI_Status status;  // return status for the receives
  
  MPI_Init(&argc,&argv);
  MPI_Comm_size(MPI_COMM_WORLD, &ncores);
  MPI_Comm_rank(MPI_COMM_WORLD, &myid);
  
  root=0;

  if(myid==root)
    printf("Ncores:= %d, root process ID:= %d\n",ncores, myid);


  if(myid == root)
    {
      // root core receives all of the messages
      for (i=1; i<ncores; i++)
	{
	  MPI_Recv(message,sizeof(message),MPI_BYTE,i,tag,MPI_COMM_WORLD,&status);
	  printf("%s\n",message);
	}
    }
  else
    {
      // create message to send to the root core
      sprintf(message,"Hello World from core %d",myid);
      dest = root;
      MPI_Send(message,strlen(message)+1,MPI_BYTE,dest,tag,MPI_COMM_WORLD);
    }
  
  // shut down MPI
  MPI_Finalize();
  
}
