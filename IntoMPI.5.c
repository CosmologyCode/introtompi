/*
 * IntoMPI.5.c: creates a virtual 2D Cartesian torus topology
 */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <mpi.h>

int main(int argc, char *argv[])
{
  int myid, ncores, root;

    MPI_Comm comm;
    int ndim, dim[2], period[2], reorder;
    int coord[2], id;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &myid);
    MPI_Comm_size(MPI_COMM_WORLD, &ncores);

    // set the root process
    root = 0;

    // usage
    if (ncores != 4)
    {
      printf("Please run with 4 processes.\n");fflush(stdout);
      MPI_Abort(MPI_COMM_WORLD, 1);
    }

    // set the number of dimensions
    ndim = 2; 
    
    // set the extent of each dimension
    dim[0]=2;    // set x-dimension
    dim[1]=2;    // set y-dimension

    // Is the topology periodic? 
    period[0]=1;  // x-dimension
    period[1]=1;  // y-dimension

    reorder=1; // reorder the cores to optimise for performance

    // generate the topology
    MPI_Cart_create(MPI_COMM_WORLD, ndim, dim, period, reorder, &comm);

    // determine the mesh coordinates of the process that has ID=5 
    if (myid == 3)
      {
        MPI_Cart_coords(comm, myid, 2, coord);
        printf("Myid %d coordinates are %d %d\n", myid, coord[0], coord[1]);
	fflush(stdout);
      }

    // the root determines the id of the process at location (3,1) in the grid
    if(myid==root)
      {
        coord[0]=1;
	coord[1]=1;
        MPI_Cart_rank(comm, coord, &id);
        printf("The processor at position (%d, %d) has myid %d\n", coord[0], coord[1], id);
	fflush(stdout);
      }

    // end MPI
    MPI_Finalize();
    return 0;
}

