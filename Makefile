#----------------------------------------------------------------------
# From the list below, please activate/deactivate the options that     
# apply to your run. If you modify any of these options, make sure     
# that you recompile the whole code by typing "make clean; make".      
#                                                                      
# Look at end of file for a brief guide to the compile-time options.   
#----------------------------------------------------------------------

EXEC   = IntoMPI.5.exe

OBJS   = IntoMPI.5.o

INCL   = Makefile

#--------------------------------------- Basic operation mode of code
#--------------------------------------- Things that are always recommended

OPT += 

#--------------------------------------- 
#----------------------------------------------------------------------
# Here, select compile environment for the target machine. This may need 
# adjustment, depending on your local system. Follow the examples to add
# additional target platforms, and to get things properly compiled.
#----------------------------------------------------------------------


#--------------------------------------- Select target computer

CC       =  mpicc                 # sets the C-compiler
OPTIMIZE =  -O2                 # sets optimization and warning flags

#--------------------------------------- Select target computer

SYSTYPE="MAC"

#--------------------------------------- Adjust settings for target computer

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ifeq ($(SYSTYPE),"MAC")
CC       =  mpicc
OPTIMIZE =  -O3 
endif

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

OPTIONS =  $(OPTIMIZE) $(OPT)

CFLAGS = $(OPTIONS) 

LIBS   =   -g   -lm 

$(EXEC): $(OBJS) 
	$(CC) $(CFLAGS)  -o  $(EXEC) $(OBJS)   $(LIBS) 

$(OBJS): $(INCL) 


clean:
	rm -f $(OBJS) $(EXEC)


